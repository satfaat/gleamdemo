# gleamdemo

[![Package Version](https://img.shields.io/hexpm/v/gleamdemo)](https://hex.pm/packages/gleamdemo)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/gleamdemo/)

```sh
gleam add gleamdemo
```
```gleam
import gleamdemo

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/gleamdemo>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```


## Web

```shell
gleam add wisp lustre mist gleam_http gleam_erlang dot_env gleam_json
```
