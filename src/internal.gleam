import argv
import envoy
import gleam/io
import gleam/result

pub fn format_pair(name: String, value: String) -> String {
  name <> "=" <> value
}

fn argv_dem() {
  case argv.load().arguments {
    ["get", name] -> get(name)
    _ -> io.println("Usage: vars get <name>")
  }
}

fn get(name: String) -> Nil {
  let value =
    envoy.get(name)
    |> result.unwrap("")
  io.println(format_pair(name, value))
}
